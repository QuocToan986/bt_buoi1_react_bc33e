import logo from "./logo.svg";
import "./App.css";
import Baitap_Buoi1 from "./Baitap_Layout_Buoi1/Baitap_Buoi1";

function App() {
  return (
    <div className="App">
      <Baitap_Buoi1 />
    </div>
  );
}

export default App;
